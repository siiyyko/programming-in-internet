const form = document.getElementById('mainForm');
const email = document.getElementById('email');
const password = document.getElementById('password');
const rePassword = document.getElementById('rePassword');
const date = document.getElementById('date');
const agreement = document.getElementById('agreement');
const errors = document.getElementById('error-messages');

function isEmailOk(email){
    let email_check = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
    if(!email.match(email_check)) return false;
    return true;
}

function isPasswordOk(password){
    if (
           password.length < 8
        || !/[A-Z]/.test(password)
        || !/[a-z]/.test(password)
        || !/\d/.test(password)
       )
    {return false;}

    return true;
}

function isPasswordsMatching(password, rePassword){
    if(password !== rePassword) return false;
    return true;
}

function isAgeOk(date){
    let minAge = new Date();
    minAge.setFullYear(minAge.getFullYear() - 16);

    let dob = new Date(date);

    return dob <= minAge;

}

function clearAll(){
    let formFields = form.querySelectorAll('input');
    formFields.forEach(function(field) {
        field.value = "";
    });
    agreement.checked = false;

    errors.innerText = '';
}

form.addEventListener('submit', (e) =>{
    let messages = [];
    if(!isEmailOk(email.value)){
        messages.push('Please, enter a correct email');
    }
    if(!isPasswordOk(password.value)){
        messages.push('Password must contain uppercase and lowercase letter, number, and must have more than 8 digits');
    }
    if(!isPasswordsMatching(password.value, rePassword.value)){
        messages.push('Passwords must match');
    }
    if(!isAgeOk(date.value)){
        messages.push('You must be over 16 years to sign up')
    }

    if(messages.length > 0){
        e.preventDefault();
        errors.innerText = messages.join(',\n');
    }
    else{
        $('#signupModal').modal('hide');
        clearAll();
    }
})