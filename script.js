let fields = [
    document.getElementById('Group'),
    document.getElementById('FirstName'),
    document.getElementById('LastName')
];

var isEditMode = false;

function changeTopFormLabel(newName){
    document.getElementById('TopFormLabel').innerText = newName;
}

function openModal() {
    document.getElementById('modalWindow').style.display = 'block';
    document.getElementById('modalBackground').style.display = 'block';
    if(isEditMode){
        changeTopFormLabel('Edit student');
    }  
    else{
        changeTopFormLabel('Add student');
    }
}

function closeModal() {
    document.getElementById('modalWindow').style.display = 'none';
    document.getElementById('modalBackground').style.display = 'none';
    isEditMode = false;

    document.getElementById('Group').value = '';
    document.getElementById('FirstName').value = '';
    document.getElementById('LastName').value = '';
    document.getElementById('Gender').value = 'Female';
    document.getElementById('Birthday').value = '';

    for(let e of fields){
        e.classList.remove('invalid-input');
    }
}

function padZero(number) {
    return number < 10 ? '0' + number : number;
}

function formatDate(date){
    let selectedDate = new Date(date.value);
    
    console.log(selectedDate);

    let day = selectedDate.getDate();
    let month = selectedDate.getMonth() + 1;
    let year = selectedDate.getFullYear();

    let formattedDate = padZero(day) + '.' + padZero(month) + '.' + year;

    return formattedDate;
}

function addStudent(){
    let group = document.getElementById('Group').value;
    let firstName = document.getElementById('FirstName').value;
    let lastName = document.getElementById('LastName').value;
    let gender = document.getElementById('Gender').value;
    let birthday = document.getElementById('Birthday');

    let info = [group, firstName, lastName];
    let indexArr = validateInfo(info);
    if(indexArr.length != 0){
        for(let i of indexArr){
            fields[i].classList.add('invalid-input');
        }
        return false;
    }
    
    let tableBody = document.getElementById('mainTable').getElementsByTagName('tbody')[0];

    let newRow = tableBody.insertRow(tableBody.rows.length);

    let checkboxCell = newRow.insertCell(0);
    let groupCell = newRow.insertCell(1);
    let nameCell = newRow.insertCell(2);
    let genderCell = newRow.insertCell(3);
    let birthdayCell = newRow.insertCell(4);
    let onlineCell = newRow.insertCell(5);
    let optionsCell = newRow.insertCell(6);

    let isOnline = Math.floor(Math.random() * 2);
    let onlineTag;
    if(isOnline){
        onlineTag = '🌹';
    }
    else{
        onlineTag = '🥀';
    }

    date = formatDate(birthday);

    let data = [group, firstName, lastName, gender, date];
    sendPost('https://ssu.gov.ua', data)

    checkboxCell.innerHTML = '<input type="checkbox">';
    groupCell.innerHTML = group;
    nameCell.innerHTML = firstName + ' ' + lastName;
    genderCell.innerHTML = gender;
    birthdayCell.innerHTML = date;
    onlineCell.innerHTML = onlineTag;

    var userOptionsDiv = document.createElement('div');
    userOptionsDiv.className = 'userOptions';
    userOptionsDiv.innerHTML = '<i class="fa-solid fa-pen" onclick="editStudent(this)"></i><i class="fa-solid fa-xmark" onclick="deleteRow(this)"></i>';
    optionsCell.appendChild(userOptionsDiv);

    closeModal();
    isEditMode = false;
}

function addRow(button){
    let isEditCorrect;
    if(!isEditMode){
        addStudent();
    }
    else{
        isEditCorrect = editRow(button);
    }
    if(isEditCorrect != false){
        console.log(isEditCorrect);
        isEditMode = false;
    }
}

function editRow(button){
    let group = document.getElementById('Group').value;
    let firstName = document.getElementById('FirstName').value;
    let lastName = document.getElementById('LastName').value;
    let gender = document.getElementById('Gender').value;
    let birthday = document.getElementById('Birthday');

    let date = formatDate(birthday);

    var closestRow = localButton.closest('tr');

    let info = [group, firstName, lastName];
    let indexArr = validateInfo(info);
    if(indexArr.length != 0){
        for(let i of indexArr){
            fields[i].classList.add('invalid-input');
        }
        return false;
    }

    closestRow.cells[1].textContent = group;
    closestRow.cells[2].textContent = firstName + ' ' + lastName;
    closestRow.cells[3].textContent = gender;
    closestRow.cells[4].textContent = date;

    let data = [group, firstName, lastName, gender, date];

    sendPost('https://ssu.gov.ua', data)

    closeModal();

}

function sendPost(url, userData){

    var requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
      };

    console.log('JSON Request:', JSON.stringify(requestOptions));

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => console.error('Помилка:', error));

}

addRedCircleClass();

function addRedCircleClass() {
    var bellIcon = document.getElementById('bell');

    setInterval(() => {
      bellIcon.classList.add('display-red-circle');

      bellIcon.addEventListener('mouseenter', function () {
        bellIcon.classList.remove('display-red-circle');
      });
    }, 10000);
}

let localButton;

function editStudent(button){

    isEditMode = true;
    localButton = button;

    var row = button.closest('tr');
    if (row) {
        var group = row.cells[1].textContent;
        var name = row.cells[2].textContent;
        var gender = row.cells[3].textContent;
        var birthday = row.cells[4].textContent;

        let genderFull;
        if(gender === 'M'){
            genderFull = "Male";
        }
        else{
            genderFull = "Female";
        }

        var nameParts = name.split(' ');

        var firstName = nameParts[0];
        var lastName = nameParts.slice(1).join(' ');

        var dateParts = birthday.split('.');
        var isoDate = dateParts[2] + '-' + dateParts[1] + '-' + dateParts[0];

        document.getElementById('Group').value = group;
        document.getElementById('FirstName').value = firstName;
        document.getElementById('LastName').value = lastName;
        document.getElementById('Gender').value = genderFull;
        document.getElementById('Birthday').value = isoDate;

        openModal();
    }
}

function validateInfo(info){
    let isName = false;
    let i = 0;
    let arr = [];
    for(const str of info){
        if(containsBannedSymbols(str, isName) || str.length == null){
            console.log(str);
            arr.push(i);
        }
        isName = true;
        i++;
    }
    return arr;
}

function containsBannedSymbols(string, isNameString){
    let regex;
    if(isNameString){
        regex = /^[a-zA-Z]+$/;
    }
    else{
        regex = /^[a-zA-Z0-9\-]+$/;
    }
    
    return !regex.test(string);
}

function deleteRow(button){
    let row = button.closest('tr');

    if(row){
        row.remove();
    }
}

let isNotifNeedToClose = false;

function showNotifications(){
    var notifBox = document.getElementById('notificationsBox');
    
    if(isNotifNeedToClose){
        notifBox.classList.remove('displayNotifBox');
    }
    else{
        notifBox.classList.add('displayNotifBox');
    }

    isNotifNeedToClose = !isNotifNeedToClose;
}

let isUserBoxNeedToClose = false;

function showUserBox(){
    var userBox = document.getElementById('userBox');

    if(isUserBoxNeedToClose){
        userBox.classList.remove('displayUserBox');
    }
    else{
        userBox.classList.add('displayUserBox')
    }

    isUserBoxNeedToClose = !isUserBoxNeedToClose;
}