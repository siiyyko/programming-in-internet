
self.addEventListener('install', function(event) {
    event.waitUntil(
      caches.open('your-cache-name').then(function(cache) {
        return cache.addAll([
          '/index.html',
          '/styles.css',
          '/script.js',
          'studentmodal.css',
          'imgs/bell.png',
          'imgs/cross.png',
          'imgs/pen.png',
          'imgs/plus.png',
          'imgs/user.png',
        ]);
      })
    );
  });
  
  self.addEventListener('fetch', function(event) {
    event.respondWith(
      caches.match(event.request).then(function(response) {
        if (response) {
          return response;
        }
  
        var fetchRequest = event.request.clone();
  
        return fetch(fetchRequest).then(function(response) {
          if(!response || response.status !== 200 || response.type !== 'basic') {
            return response;
          }
  
          var responseToCache = response.clone();
  
          caches.open('your-cache-name').then(function(cache) {
            cache.put(event.request, responseToCache);
          });
  
          return response;
        });
      })
    );
  });
  